import React from 'react';
import PropTypes from 'prop-types';

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link rel="stylesheet" href="/fonts/Feather/feather.css" />
        <link
          rel="stylesheet"
          href="/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css"
        />
        <link rel="stylesheet" href="/libs/aos/dist/aos.css" />
        <link
          rel="stylesheet"
          href="/libs/choices.js/public/assets/styles/choices.min.css"
        />
        <link rel="stylesheet" href="/libs/flickity-fade/flickity-fade.css" />
        <link rel="stylesheet" href="/libs/flickity/dist/flickity.min.css" />
        <link rel="stylesheet" href="/libs/highlightjs/styles/vs2015.css" />
        <link rel="stylesheet" href="/libs/jarallax/dist/jarallax.css" />
        <link rel="stylesheet" href="/libs/quill/dist/quill.core.css" />
        <title>Ober</title>
        <meta
          property="og:title"
          content="Ober: de digitale collega voor elke horecazaak!"
        />
        <meta
          property="og:description"
          content="Met Ober kunnen jouw gasten op een veilige manier digitaal bestellen en meteen ook betalen. Zo kan jij heel eenvoudig je horecazaak futureproof organiseren. Ober is veilig, contactloos en efficiënt. Vervolledig nu je team met Ober!"
        />
        <meta
          property="og:image"
          content="https://ober.digital/img/vid_bg.jpg"
        />
        <meta property="og:url" content="https://ober.digital" />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />

        {props.postBodyComponents}
        <script src="/libs/jquery/dist/jquery.min.js" />
        <script src="/libs/bootstrap/dist/js/bootstrap.bundle.min.js" />
        <script src="/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js" />
        <script src="/libs/aos/dist/aos.js" />
        <script src="/libs/choices.js/public/assets/scripts/choices.min.js" />
        <script src="/libs/countup.js/dist/countUp.min.js" />
        <script src="/libs/dropzone/dist/min/dropzone.min.js" />
        <script src="/libs/flickity/dist/flickity.pkgd.min.js" />
        <script src="/libs/flickity-fade/flickity-fade.js" />
        <script src="/libs/highlightjs/highlight.pack.min.js" />
        <script src="/libs/imagesloaded/imagesloaded.pkgd.min.js" />
        <script src="/libs/isotope-layout/dist/isotope.pkgd.min.js" />
        <script src="/libs/jarallax/dist/jarallax.min.js" />
        <script src="/libs/jarallax/dist/jarallax-video.min.js" />
        <script src="/libs/jarallax/dist/jarallax-element.min.js" />
        <script src="/libs/quill/dist/quill.min.js" />
        <script src="/libs/smooth-scroll/dist/smooth-scroll.min.js" />
        <script src="/libs/typed.js/lib/typed.min.js" />

        <script src="/js/theme.min.js" />
        <script
          dangerouslySetInnerHTML={{
            __html: `
            var elements = document.querySelectorAll(".intercombtn");
            for (var i = 0; i < elements.length; i++) {
              elements[i].addEventListener("click", function(e) {
                e.preventDefault();
                if (typeof window !== 'undefined') { 
                  window.Intercom('show')
                }
              });
            }`,
          }}
        ></script>
      </body>
    </html>
  );
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
};
