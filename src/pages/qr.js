import React, { useState } from 'react';
import sticker from '../images/sticker.svg';
// import '../components/qr.css';
import QRCode from 'qrcode.react';
import { v4 as uuidv4 } from 'uuid';

function QR() {
  const [qrs, setQrs] = useState([]);

  React.useEffect(() => {
    setQrs([...Array(30)].map(() => [...Array(8)].map(() => uuidv4())));
  }, []);

  return qrs.map((page, cid) => (
    <div key={cid} className="stickercontainer">
      <div className="pageyellowline" />
      {page.map((qr, sid) => (
        <div key={sid} className="sticker">
          <div className="yellowline" />
          <img src={sticker} className="stickerimg" alt="sticker" />
          <QRCode
            height="30mm"
            width="30mm"
            fgColor="#283E46"
            bgColor="transparent"
            renderAs="svg"
            value={`https://ober.digital/${qr}`}
          />
        </div>
      ))}
    </div>
  ));
}

export default QR;
