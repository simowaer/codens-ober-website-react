import React, { useState } from 'react';
import sticker from '../images/sticker.svg';
import '../components/qr2.css';
import QRCode from 'qrcode.react';
import { v4 as uuidv4 } from 'uuid';

function QR() {
  const [qrs, setQrs] = useState([]);

  React.useEffect(() => {
    setQrs([...Array(70)].map(() => [...Array(15)].map(() => uuidv4())));
  }, []);

  return qrs.map((page, cid) => (
    <div key={cid} className="stickercontainer2">
      <div className="pageyellowline2" />
      {page.map((qr, sid) => (
        <div key={sid} className="sticker2">
          <div className="yellowline2" />
          <img src={sticker} className="stickerimg2" alt="sticker" />
          <QRCode
            height="25mm"
            width="25mm"
            fgColor="#283E46"
            bgColor="transparent"
            renderAs="svg"
            value={`https://ober.digital/${qr}`}
          />
        </div>
      ))}
    </div>
  ));
}

export default QR;
