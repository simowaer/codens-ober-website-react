import React from 'react';
import Header from '../components/header';
import Footer from '../components/footer';

import '../components/layout.css';
import '../components/tarieven.css';

const TarievenPage = () => (
  <div>
    <Header />
    <section
      className="pt-8 pt-md-11 pb-10 pb-md-15"
      style={{ backgroundColor: '#eef2f5' }}
    >
      {/* Shape */}
      <div className="shape shape-blur-3 svg-shim text-white">
        <svg
          viewBox="0 0 1738 487"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M0 0h1420.92s713.43 457.505 0 485.868C707.502 514.231 0 0 0 0z"
            fill="url(#paint0_linear)"
          />
          <defs>
            <linearGradient
              id="paint0_linear"
              x1={0}
              y1={0}
              x2="1049.98"
              y2="912.68"
              gradientUnits="userSpaceOnUse"
            >
              <stop stopColor="currentColor" stopOpacity=".075" />
              <stop offset={1} stopColor="currentColor" stopOpacity={0} />
            </linearGradient>
          </defs>
        </svg>
      </div>
      {/* Content */}
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 text-center">
            {/* Heading */}
            <h1 className="display-2">Simpele, transparante prijzen.</h1>
            {/* Text */}
            <p className="lead mb-6 mb-md-4">
              We hebben een prijs op maat van elke zaak.
            </p>
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    {/* SHAPE
================================================== */}
    <div className="position-relative">
      <div className="shape shape-bottom shape-fluid-x svg-shim text-light">
        <svg
          viewBox="0 0 2880 48"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M0 48h2880V0h-720C1442.5 52 720 0 720 0H0v48z" fill="#fff" />
        </svg>
      </div>
    </div>
    {/* PRICING
================================================== */}
    <section className="mt-n8 mt-md-n14">
      <div className="container pricing">
        <div className="form-row">
          <div className="col-12 col-md-3">
            {/* Card */}
            <div className="card shadow-lg mb-6 mb-md-0">
              <div className="card-body" style={{ padding: '1rem' }}>
                {/* Preheading */}
                <div className="text-center mb-3">
                  <span className="badge badge-pill badge-primary-soft">
                    <span className="h6 text-uppercase">Basic</span>
                  </span>
                </div>
                {/* Price */}
                <div className="d-flex justify-content-center">
                  <span className="h2 mb-0 mt-2">€</span>
                  <span
                    className="price display-2 mb-0"
                    data-annual={0}
                    data-monthly={0}
                  >
                    45
                  </span>
                  <span className="h2 align-self-end mb-1">/maand</span>
                </div>
                {/* Text */}
                <p className="text-center text-muted mb-5">excl. btw</p>
                {/* List */}
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Digitale en dynamische menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Beheermodule voor de menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Persoonlijke service en online ondersteuning</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Abonnement kan gepauzeerd worden</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Geen opstartkost (inclusief gratis QR&#8209;codes)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Bestelmodule (klanten kunnen hun wensen doorgeven)
                  </p>
                </div>
                {/* Button */}
                <a
                  href="#!"
                  className="intercombtn btn btn-block btn-primary-soft"
                >
                  Start met basic
                </a>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-3">
            {/* Card */}
            <div className="card shadow-lg mb-6 mb-md-0">
              <div className="card-body" style={{ padding: '1rem' }}>
                {/* Preheading */}
                <div className="text-center mb-3">
                  <span className="badge badge-pill badge-primary-soft">
                    <span className="h6 text-uppercase">Standaard</span>
                  </span>
                </div>
                {/* Price */}
                <div className="d-flex justify-content-center">
                  <span className="h2 mb-0 mt-2">€</span>
                  <span
                    className="price display-2 mb-0"
                    data-annual={29}
                    data-monthly={49}
                  >
                    59
                  </span>
                  <span className="h2 align-self-end mb-1">/maand</span>
                </div>
                {/* Text */}
                <p className="text-center text-muted mb-5">excl. btw</p>
                {/* List */}
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Digitale en dynamische menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Beheermodule voor de menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Persoonlijke service en online ondersteuning</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Abonnement kan gepauzeerd worden</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Geen opstartkost (inclusief gratis QR&#8209;codes)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Bestelmodule (klanten kunnen hun wensen doorgeven)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Betaalmodule (Cash, Kaart, Payconiq of Mollie)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">Tot 25 tafels</p>
                </div>
                {/* Button */}
                <a href="#!" className="intercombtn btn btn-block btn-primary">
                  Start met Standaard
                </a>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-3">
            {/* Card */}
            <div className="card shadow-lg mb-md-0">
              <div className="card-body" style={{ padding: '1rem' }}>
                {/* Preheading */}
                <div className="text-center mb-3">
                  <span className="badge badge-pill badge-primary-soft">
                    <span className="h6 text-uppercase">Standaard+</span>
                  </span>
                </div>
                {/* Price */}
                <div className="d-flex justify-content-center">
                  <span className="h2 mb-0 mt-2">€</span>
                  <span
                    className="price display-2 mb-0"
                    data-annual={49}
                    data-monthly={69}
                  >
                    75
                  </span>
                  <span className="h2 align-self-end mb-1">/maand</span>
                </div>
                {/* Text */}
                <p className="text-center text-muted mb-5">excl. btw</p>
                {/* List */}
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Digitale en dynamische menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Beheermodule voor de menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Persoonlijke service en online ondersteuning</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Abonnement kan gepauzeerd worden</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Geen opstartkost (inclusief gratis QR&#8209;codes)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Bestelmodule (klanten kunnen hun wensen doorgeven)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Betaalmodule (Cash, Kaart, Payconiq of Mollie)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">Tot 50 tafels</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">Meerdere talen</p>
                </div>
                {/* Button */}
                <a
                  href="#!"
                  className="intercombtn btn btn-block btn-primary-soft"
                >
                  Start met Standaard+
                </a>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-3">
            {/* Card */}
            <div className="card shadow-lg mb-md-0">
              <div className="card-body" style={{ padding: '1rem' }}>
                {/* Preheading */}
                <div className="text-center mb-3">
                  <span className="badge badge-pill badge-primary-soft">
                    <span className="h6 text-uppercase">Premium</span>
                  </span>
                </div>
                {/* Price */}
                <div className="d-flex justify-content-center">
                  <span className="h2 mb-0 mt-2">€</span>
                  <span
                    className="price display-2 mb-0"
                    data-annual={49}
                    data-monthly={69}
                  >
                    89
                  </span>
                  <span className="h2 align-self-end mb-1">/maand</span>
                </div>
                {/* Text */}
                <p className="text-center text-muted mb-5">excl. btw</p>
                {/* List */}
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Digitale en dynamische menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Beheermodule voor de menukaart</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Persoonlijke service en online ondersteuning</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p>Abonnement kan gepauzeerd worden</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Geen opstartkost (inclusief gratis QR&#8209;codes)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Bestelmodule (klanten kunnen hun wensen doorgeven)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">
                    Betaalmodule (Cash, Kaart, Payconiq of Mollie)
                  </p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">Meer dan 50 tafels</p>
                </div>
                <div className="d-flex">
                  {/* Badge */}
                  <div className="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                    <i className="fe fe-check" />
                  </div>
                  {/* Text */}
                  <p className="mb-5">Meerdere talen</p>
                </div>
                {/* Button */}
                <a
                  href="#!"
                  className="intercombtn btn btn-block btn-primary-soft"
                >
                  Start met Premium
                </a>
              </div>
            </div>
          </div>
        </div>{' '}
        <section className="py-8 py-md-11">
          <div className="container">
            <div className="row justify-content-center mb-8">
              <div className="col-12 col-md-10 col-lg-8 text-center">
                {/* Badge */}
                {/* Heading */}
                <h3>
                  QR&#8209;codes die na de opstartkost worden bijbesteld kosten{' '}
                  <br />
                  <span className="font-weight-bold text-primary">
                    €1/stuk excl. btw
                  </span>
                  .
                </h3>
                {/* Text */}
              </div>
            </div>{' '}
            {/* / .row */}
            <div className="row">
              <div className="col-12 col-md-4 text-center">
                {/* Heading */}
                <p>
                  Abonnementen kunnen ten vroegste opgezegd worden na 4 maanden.
                </p>
              </div>
              <div className="col-12 col-md-4 text-center">
                {/* Heading */}
                <p>Deze prijzen zijn geldig voor het kalenderjaar 2020.</p>
                {/* Text */}
              </div>
              <div className="col-12 col-md-4 text-center">
                <p>Algemene voorwaarden zijn geldig.</p>
                {/* Text */}
              </div>
            </div>{' '}
            {/* / .row */}
          </div>{' '}
          {/* / .container */}
        </section>
      </div>{' '}
      {/* / .container */}
    </section>

    <Footer />
  </div>
);

export default TarievenPage;
