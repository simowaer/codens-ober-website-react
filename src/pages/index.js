import React from 'react';
import Header from '../components/header';
import Footer from '../components/footer';

import '../components/layout.css';

const IndexPage = () => (
  <div>
    <Header />
    <section className="pt-6 pt-md-8">
      <div className="container">
        <div className="row align-items-center justify-content-center justify-content-md-between">
          <div className="col-10 col-sm-8 col-md-6 order-md-2">
            {/* iPhone + iPhone */}
            <div className="device-combo device-combo-iphonex-iphonex mb-6 mb-md-0">
              {/* iPhone */}
              <div className="device device-iphonex" data-aos="fade-left">
                <img
                  src="/ober_app_1.png"
                  className="device-screen"
                  alt="Overzichtscherm"
                />
                <img
                  src="/img/devices/iphonex.svg"
                  className="img-fluid"
                  alt="iphone frame"
                />
              </div>
              {/* iPhone */}
              <div
                className="device device-iphonex"
                data-aos="fade-left"
                data-aos-delay={150}
              >
                <img
                  src="/ober_app_2.png"
                  className="device-screen"
                  alt="Bestel scherm"
                />
                <img
                  src="/img/devices/iphonex.svg"
                  className="img-fluid"
                  alt="iphone frame"
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-5" data-aos="fade-right">
            {/* Heading */}
            <h1 className="font-weight-bold">
              Ober: de digitale collega voor elke horecazaak!
            </h1>
            {/* Text */}
            <p className="font-size-lg text-muted mb-6 mb-md-8">
              Met Ober kunnen jouw gasten op een veilige manier digitaal
              bestellen en meteen ook betalen. Zo kan jij heel eenvoudig je
              horecazaak futureproof organiseren. Ober is veilig, contactloos en
              efficiënt. Vervolledig nu je team met Ober!
            </p>
            {/* Form */}
            <a href="#" className="intercombtn btn btn-primary mr-3 lift">
              Start gratis <i className="fe fe-arrow-right ml-3"></i>
            </a>
            <a href="#hoewerkthet" className="btn btn-primary-soft lift">
              Hoe werkt het?
            </a>
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    {/* ABOUT
================================================== */}
    <section id="hoewerkthet" className="pt-8 pt-md-11">
      <div className="container">
        <div className="row align-items-center justify-content-between">
          <div className="col-12 col-md-6 mb-5 mb-md-0">
            {/* Images */}
            <div className="row">
              <div className="col-6 mr-n5">
                <img
                  src="/img/1_scan.jpg"
                  alt="Scan de QR code"
                  className="img-fluid mb-4 rounded"
                  data-aos="fade-right"
                  data-aos-delay={100}
                />
                <img
                  src="/img/2_order.jpg"
                  alt="Bestel je drankje"
                  className="img-fluid rounded"
                  data-aos="fade-right"
                  data-aos-delay={150}
                />
              </div>
              <div className="col-6 mt-8">
                <img
                  src="/img/3_pay.jpg"
                  alt="Bestelling geplaatst"
                  className="img-fluid mb-4 rounded"
                  data-aos="fade-right"
                />
                <img
                  src="/img/4_enjoy.jpg"
                  alt="Geniet"
                  className="img-fluid rounded"
                  data-aos="fade-right"
                  data-aos-delay={50}
                />
              </div>
            </div>{' '}
            {/* / .row */}
          </div>
          <div className="col-12 col-md-6 col-lg-5" data-aos="fade-left">
            {/* Heading */}
            <h2 className="font-weight-bold">Hoe werkt het?</h2>
            {/* Text */}
            <p className="font-size-lg text-muted mb-4">
              Je gast scant de QR-code en maakt een keuze uit de
              gedigitaliseerde menukaart. Jij krijgt de bestelling realtime te
              zien en kan er meteen mee aan de slag. Betalen gebeurt digitaal en
              dus ook contactloos. Zo zorgen we samen voor een veilige én
              efficiënte werking. Voor je gasten en voor jou!
            </p>
            {/* Button */}
            <button className="intercombtn btn btn-primary">Meer weten?</button>
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    {/* ABOUT
================================================== */}
    <section id="voordelen" className="py-8 py-md-11">
      <div className="container">
        <div className="row align-items-center justify-content-between">
          <div className="col-12 col-md-6 order-md-2">
            {/* Card */}
            <div className="card card-border border-success shadow-lg">
              <div className="card-body">
                {/* List group */}
                <div className="list-group list-group-flush">
                  <div className="list-group-item d-flex align-items-center">
                    {/* Text */}
                    <div className="mr-auto">
                      {/* Heading */}
                      <p className="font-weight-bold mb-1">Easy to use</p>
                      {/* Text */}
                      <p className="font-size-sm text-muted mb-0">
                        Alle gasten met een smartphone kunnen bestellen en
                        betalen: geen login, registratie of app nodig!
                        <br />
                      </p>
                    </div>
                    {/* Check */}
                    <div className="badge badge-rounded-circle badge-success-soft ml-4">
                      <i className="fe fe-check" />
                    </div>
                  </div>
                  <div className="list-group-item d-flex align-items-center">
                    {/* Text */}
                    <div className="mr-auto">
                      {/* Heading */}
                      <p className="font-weight-bold mb-1">
                        Bewaar afstand: contactloos en digitaal
                      </p>
                      {/* Text */}
                      <p className="font-size-sm text-muted mb-0">
                        Je gasten bestellen en betalen digitaal, er is geen
                        contact nodig met menukaarten, cash geld of
                        betaalterminals. Zo bewaren we samen een veilige
                        afstand.
                      </p>
                    </div>
                    {/* Check */}
                    <div className="badge badge-rounded-circle badge-success-soft ml-4">
                      <i className="fe fe-check" />
                    </div>
                  </div>
                  <div className="list-group-item d-flex align-items-center">
                    {/* Text */}
                    <div className="mr-auto">
                      {/* Heading */}
                      <p className="font-weight-bold mb-1">Gratis opstart</p>
                      {/* Text */}
                      <p className="font-size-sm text-muted mb-0">
                        Starten met Ober is kosteloos. De betaling start als
                        jouw gasten het platform ook effectief gebruiken!
                      </p>
                    </div>
                    {/* Check */}
                    <div className="badge badge-rounded-circle badge-success-soft ml-4">
                      <i className="fe fe-check" />
                    </div>
                  </div>
                  <div className="list-group-item d-flex align-items-center">
                    {/* Text */}
                    <div className="mr-auto">
                      {/* Heading */}
                      <p className="font-weight-bold mb-1">Partnership</p>
                      {/* Text */}
                      <p className="font-size-sm text-muted mb-0">
                        We zorgen voor de nodige ondersteuning en we bieden een
                        flexibel abonnement aan. Ben je in verlof of gebruik je
                        ons platform even niet? Geen probleem, zet je abonnement
                        tijdelijk op pauze zonder extra kosten!
                      </p>
                    </div>
                    {/* Check */}
                    <div className="badge badge-rounded-circle badge-success-soft ml-4">
                      <i className="fe fe-check" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 order-md-1">
            {/* Badge */}
            <span className="badge badge-success-soft badge-pill mb-3">
              <span className="h6 text-uppercase">Voordelen</span>
            </span>
            {/* Heading */}
            <h2>
              Hoe start je met ober? <br />
              <span className="text-success">
                Kleef de QR-stickers, geef je menu- of drankkaart in en that’s
                it!
              </span>
            </h2>
            {/* Text */}
            <p className="font-size-lg text-muted mb-6 mb-md-0">
              Je gasten kiezen een tafeltje, selecteren hun bestelling en kunnen
              dan achterover leunen. Jij krijgt de bestelling realtime door en
              kan er meteen mee aan de slag. Zo zorgen we er samen voor dat
              iedereen terug veilig kan genieten in jouw horeca-zaak!
            </p>
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    {/* SHAPE
================================================== */}
    <div className="position-relative">
      <div className="shape shape-top shape-fluid-x svg-shim text-white">
        <svg
          viewBox="0 0 2880 72"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M2880 0H0v54.112h720s289.42 40.248 705 0c416.5-37.733 735 0 735 0h720V0z"
            fill="currentColor"
          />
        </svg>
      </div>
    </div>
    {/* VIDEO
================================================== */}
    <section
      id="bekijkober"
      className="py-12 py-md-13 bg-cover"
      style={{ backgroundImage: 'url(/img/vid_bg.jpg)' }}
    >
      <div className="container py-md-13">
        <div className="row">
          <div className="col-12 text-center">
            {/* Button */}
            {/* <a
          href="https://www.youtube.com/watch?v=9I-Y6VQ6tyI"
          className="btn btn-pill btn-white text-body shadow lift"
          data-fancybox
        >
          <span className="h6 text-uppercase font-weight-bold">
            <i className="fe fe-play mr-2" /> Bekijk ober
          </span>
        </a> */}
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    {/* CTA
================================================== */}
    <section
      className="py-10 py-md-13 bg-gray-200 bg-between"
      style={{
        backgroundImage:
          'url(assets/img/illustrations/illustration-1-cropped.png), url(assets/img/illustrations/illustration-3-cropped.png)',
      }}
    >
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 text-center">
            {/* Heading */}
            <h2 className="font-weight-bold">
              Probeer ober uit. Wij komen naar jouw zaak.
            </h2>
            {/* Text */}
            <p className="font-size-lg text-muted mb-8 px-lg-9">
              Veilig voor je klanten, voor jezelf én je personeel.
            </p>
            {/* Button */}
            <button className="intercombtn btn btn-primary lift">
              Start gratis
            </button>
          </div>
        </div>{' '}
        {/* / .row */}
      </div>{' '}
      {/* / .container */}
    </section>
    <Footer />
  </div>
);

export default IndexPage;
