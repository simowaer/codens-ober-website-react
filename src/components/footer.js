import React from 'react';

import { Link } from 'gatsby';

const Footer = () => (
  <footer className="py-8 py-md-11 bg-white">
    <div className="container">
      <div className="row">
        <div className="col-12 col-md-4 col-lg-4">
          {/* Brand */}
          <img
            src={'/img/brand.svg'}
            alt="Ober, uw digitale collega"
            className="footer-brand img-fluid mb-2"
          />
          {/* Text */}
          <p className="text-gray-700 mb-2">Jouw digitale collega.</p>
          {/* Social */}
          <ul className="list-unstyled list-inline list-social mb-6 mb-md-0">
            <li className="list-inline-item list-social-item mr-3">
              <a
                href="https://www.instagram.com/ober.digital/"
                className="text-decoration-none"
              >
                <img
                  src={'/img/icons/social/instagram.svg'}
                  className="list-social-icon"
                  alt="Ober on Instagram"
                />
              </a>
            </li>
            <li className="list-inline-item list-social-item mr-3">
              <a
                href="https://www.facebook.com/OberOnline/"
                className="text-decoration-none"
              >
                <img
                  src={'/img/icons/social/facebook.svg'}
                  className="list-social-icon"
                  alt="Ober on Facebook"
                />
              </a>
            </li>
          </ul>
          <p>© {new Date().getFullYear()} Ober</p>
        </div>
        <div className="col-6 col-md-4 col-lg-4">
          {/* Heading */}
          <h6 className="font-weight-bold text-uppercase text-gray-700">
            Ober
          </h6>
          {/* List */}
          <ul className="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
            <li className="mb-3">
              <Link to="/" className="text-reset">
                Digitale collega
              </Link>
            </li>
            <li className="mb-3">
              <Link to="/#hoewerkthet" className="text-reset">
                Hoe werkt het?
              </Link>
            </li>
            <li className="mb-3">
              <Link to="/#voordelen" className="text-reset">
                Voordelen
              </Link>
            </li>
            {/* <li className="mb-3">
      <a href="#bekijkober" className="text-reset">
        Bekijk ober
      </a>
    </li> */}
          </ul>
        </div>
        <div className="col-6 col-md-4 col-lg-4">
          {/* Heading */}
          <h6 className="font-weight-bold text-uppercase text-gray-700">
            Neem contact op
          </h6>
          {/* List */}
          <ul className="list-unstyled text-muted mb-0">
            <li className="mb-3">
              <a href="mailto:ober@codens.be" className="text-reset">
                ober@codens.be
              </a>
            </li>
            <li className="mb-3">
              <a href="tel:+32494416049" className="text-reset">
                +32 494 41 60 49
              </a>
            </li>
            <li className="mb-3">
              of{' '}
              <a href="#" className="intercombtn text-reset">
                via chat
              </a>{' '}
            </li>
          </ul>
        </div>
      </div>{' '}
      {/* / .row */}
    </div>{' '}
    {/* / .container */}
  </footer>
);

export default Footer;
