import React from 'react';

import { Link } from 'gatsby';

const Header = () => (
  <nav
    id="digitalecollega"
    className="navbar navbar-expand-lg navbar-light bg-white"
  >
    <div className="container-fluid">
      {/* Brand */}
      <Link className="navbar-brand" to="/">
        <img src="/img/brand.svg" className="navbar-brand-img" alt="Ober" />
      </Link>
      {/* Toggler */}
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarCollapse"
        aria-controls="navbarCollapse"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      {/* Collapse */}
      <div className="collapse navbar-collapse" id="navbarCollapse">
        {/* Toggler */}
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarCollapse"
          aria-controls="navbarCollapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="fe fe-x" />
        </button>
        {/* Navigation */}
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/">
              Digitale collega
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/#hoewerkthet">
              Hoe werkt het?
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/#voordelen">
              Voordelen
            </Link>
          </li>
          {/* <li className="nav-item">
            <a className="nav-link" href="#bekijkober">
              Bekijk Ober
            </a>
          </li> */}
        </ul>
        {/* Button */}
        <a
          href="#"
          className="intercombtn navbar-btn btn btn-sm btn-primary lift ml-auto"
        >
          Chat met ons
        </a>
      </div>
    </div>
  </nav>
);

export default Header;
