module.exports = {
  siteMetadata: {
    title: `Ober`,
    description: `Met Ober ben je klaar om de uitdagingen een antwoord te bieden! Deze digitale helper zal het voor horecazaken veilig en makkelijk maken om bestellingen te verwerken en betalingen te ontvangen.`,
    author: `@ober`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ober`,
        short_name: `ober`,
        start_url: `/`,
        background_color: `#273e46`,
        theme_color: `#273e46`,
        display: `minimal-ui`,
        icon: `src/images/ober.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: 'UA-88240257-9',
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
      },
    },
    {
      resolve: `gatsby-plugin-intercom`,
      options: {
        appId: 'hrohzoo5',
      },
    },
    `@wardpeet/gatsby-plugin-static-site`,
    `gatsby-plugin-client-side-redirect`, // keep it in last in list
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
